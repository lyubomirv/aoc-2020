export function task(): void {
	const testInput1: string[] = [
		"939",
		"7,13,x,x,59,x,31,19"
	];

	const testInput2: string[] = [
		"1",
		"17,x,13,19"
	];

	const testInput3: string[] = [
		"1",
		"67,7,59,61"
	];

	const testInput4: string[] = [
		"1",
		"67,x,7,59,61"
	];

	const testInput5: string[] = [
		"1",
		"67,7,x,59,61"
	];

	const testInput6: string[] = [
		"1",
		"1789,37,47,1889"
	];

	const testInput7: string[] = [
		"1",
		"3, 2"
	];

	const testInput8: string[] = [
		"1",
		"2, 3, 5, 7"
	];

	const input: string[] = [
		"1002462",
		"37,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,41,x,x,x,x,x,x,x,x,x,601,x,x,x,x,x,x,x,x,x,x,x,19,x,x,x,x,17,x,x,x,x,x,23,x,x,x,x,x,29,x,443,x,x,x,x,x,x,x,x,x,x,x,x,13"
	];

	function part1(input: string[]): number {
		const timestamp: number = Number(input[0]);
		const busIds = input[1].split(',').filter(e => e !== 'x').map(e => Number(e));

		const earliestBusTimestamps = busIds.map(id => (Math.floor(timestamp / id) + 1) * id);
		const earliestBusTimestamp = earliestBusTimestamps.reduce((prev, curr) => Math.min(prev, curr), Number.MAX_SAFE_INTEGER);
		const busId = busIds[earliestBusTimestamps.indexOf(earliestBusTimestamp)];

		return busId * (earliestBusTimestamp - timestamp);
	}

	interface BusInfo {
		id: number;
		offset: number;
	}

	function findFirstTimestamp(busInfos: BusInfo[], step: number, start: number): number {
		let result: number = 0;

		for(let timestamp = start; ; timestamp += step) {
			let found = true;
			for(const busInfo of busInfos) {
				if(timestamp < busInfo.id) {
					found = false;
					break;
				}

				if((timestamp + busInfo.offset) % busInfo.id !== 0) {
					found = false;
					break;
				}
			}

			if(found) {
				result = timestamp;
				break;
			}
		}

		return result;
	}

	// Explanation (figured out by doing some maths on a piece of paper)
	//  - find the first timestamp of 2 numbers, using the first of them as the step
	//  - calculate the new step like this: oldStep * the 2nd number
	//  - find the first timestamp of those 2 numbers and a 3rd one
	//  - calculate the new step like this: oldStep * the 3rd number
	//  - repeat until the end of the numbers
	function part2(input: string[]): number {
		const busIds: number[] = input[1].split(',').map(e => e === 'x' ? 0 : Number(e));
		const busInfos: BusInfo[] = busIds
			.map((e, index) => { return { id: e, offset: index }; })
			.filter(e => e.id !== 0);
		busInfos.sort((a, b) => b.id - a.id);
		console.log(busInfos);

		const totalCount: number = busInfos.length;
		let firstTimestamp: number = busInfos[0].id - busInfos[0].offset;
		let step: number = busInfos[0].id;
		for(let size = 2; size <= totalCount; ++size) {
			firstTimestamp = findFirstTimestamp(busInfos.slice(0, size), step, firstTimestamp);
			step = step * busInfos[size - 1].id;
		}

		return firstTimestamp;
	}

	function task(input: string[]): void {
		console.log(`Part 1: ${part1(input)}`);
		console.log(`Part 2: ${part2(input)}`);
	}

	// task(testInput8);
	task(input);
}
