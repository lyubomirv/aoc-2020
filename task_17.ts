export function task(): void {
	const testInput: string[] = [
		'.#.',
		'..#',
		'###'];

	const input: string[] = [
		'##.#...#',
		'#..##...',
		'....#..#',
		'....####',
		'#.#....#',
		'###.#.#.',
		'.#.#.#..',
		'.#.....#'];

	type Data3D = number[][][];
	type Data4D = number[][][][];

	class Space3D {
		initialSide: number = 0;
		totalSizeXY: number = 0;
		totalSizeZ: number = 0;

		private data: Data3D = [];

		constructor(input: string[], steps: number) {
			this.initialSide = input.length;
			this.totalSizeXY = this.initialSide + 2 * steps;
			this.totalSizeZ = 1 + 2 * steps;

			this.data = this.initData();

			for(let x = 0; x < input.length; ++x) {
				for(let y = 0; y < input[x].length; ++y) {
					const value = input[x][y] === '.' ? 0 : 1;
					this.setState(x, y, 0, value);
				}
			}
		}

		public setState(x: number, y: number, z: number, value: number): void {
			const baseX = (this.totalSizeXY - this.initialSide) / 2;
			const baseY = (this.totalSizeXY - this.initialSide) / 2;
			const baseZ = (this.totalSizeZ - 1) / 2;

			this.data[baseX + x][baseY + y][baseZ + z] = value;;
		}

		public getActiveCount(): number {
			let count: number = 0;
			for(let x = 0; x < this.data.length; ++x) {
				for(let y = 0; y < this.data[x].length; ++y) {
					for(let z = 0; z < this.data[x][y].length; ++z) {
						if(this.data[x][y][z] === 1) {
							count += 1;
						}
					}
				}
			}
			return count;
		}

		public nextState(): void {
			const newData: Data3D = this.initData();

			for(let x = 0; x < this.data.length; ++x) {
				for(let y = 0; y < this.data[x].length; ++y) {
					for(let z = 0; z < this.data[x][y].length; ++z) {
						newData[x][y][z] = this.getNextCellState(x, y, z);
					}
				}
			}

			this.data = newData;
		}

		private initData(): Data3D {
			const data: Data3D = [];
			for(let x = 0; x < this.totalSizeXY; ++x) {
				data.push([]);
				const row = data[data.length - 1];
				for(let y = 0; y < this.totalSizeXY; ++y) {
					row.push([]);
					const col = row[row.length - 1];
					for(let z = 0; z < this.totalSizeZ; ++z) {
						col.push(0);
					}
				}
			}
			return data;
		}

		private getNextCellState(x: number, y: number, z: number): number {
			let countInactive: number = 0;
			let countActive: number = 0;
			for(const i of [x - 1, x, x + 1]) {
				for(const j of [y - 1, y, y + 1]) {
					for(const k of [z - 1, z, z + 1]) {
						if(i === x && j === y && k === z) {
							continue;
						}

						if(i < 0 || j < 0 || k < 0
							|| i >= this.totalSizeXY || j >= this.totalSizeXY || k >= this.totalSizeZ) {
							countInactive += 1;
							continue;
						}

						if(this.data[i][j][k] === 1) {
							countActive += 1;
						} else {
							countInactive += 1;
						}
					}
				}
			}

			if(this.data[x][y][z] === 1) {
				if(countActive === 2 || countActive === 3) {
					return 1;
				} else {
					return 0;
				}
			}

			if(countActive === 3) {
				return 1;
			}

			return 0;
		}
	}

	class Space4D {
		initialSide: number = 0;
		totalSizeXY: number = 0;
		totalSizeZW: number = 0;

		private data: Data4D = [];

		constructor(input: string[], steps: number) {
			this.initialSide = input.length;
			this.totalSizeXY = this.initialSide + 2 * steps;
			this.totalSizeZW = 1 + 2 * steps;

			this.data = this.initData();

			for(let x = 0; x < input.length; ++x) {
				for(let y = 0; y < input[x].length; ++y) {
					const value = input[x][y] === '.' ? 0 : 1;
					this.setState(x, y, 0, 0, value);
				}
			}
		}

		public setState(x: number, y: number, z: number, w: number, value: number): void {
			const baseX = (this.totalSizeXY - this.initialSide) / 2;
			const baseY = (this.totalSizeXY - this.initialSide) / 2;
			const baseZ = (this.totalSizeZW - 1) / 2;
			const baseW = (this.totalSizeZW - 1) / 2;

			this.data[baseX + x][baseY + y][baseZ + z][baseW + w] = value;;
		}

		public getActiveCount(): number {
			let count: number = 0;
			for(let x = 0; x < this.data.length; ++x) {
				for(let y = 0; y < this.data[x].length; ++y) {
					for(let z = 0; z < this.data[x][y].length; ++z) {
						for(let w = 0; w < this.data[x][y][z].length; ++w) {
							if(this.data[x][y][z][w] === 1) {
								count += 1;
							}
						}
					}
				}
			}
			return count;
		}

		public nextState(): void {
			const newData: Data4D = this.initData();

			for(let x = 0; x < this.data.length; ++x) {
				for(let y = 0; y < this.data[x].length; ++y) {
					for(let z = 0; z < this.data[x][y].length; ++z) {
						for(let w = 0; w < this.data[x][y][z].length; ++w) {
							newData[x][y][z][w] = this.getNextCellState(x, y, z, w);
						}
					}
				}
			}

			this.data = newData;
		}

		private initData(): Data4D {
			const data: Data4D = [];
			for(let x = 0; x < this.totalSizeXY; ++x) {
				data.push([]);
				const row = data[data.length - 1];
				for(let y = 0; y < this.totalSizeXY; ++y) {
					row.push([]);
					const col = row[row.length - 1];
					for(let z = 0; z < this.totalSizeZW; ++z) {
						col.push([]);
						const smth = col[col.length - 1];
						for(let w = 0; w < this.totalSizeZW; ++w) {
							smth.push(0);
						}
					}
				}
			}
			return data;
		}

		private getNextCellState(x: number, y: number, z: number, w: number): number {
			let countInactive: number = 0;
			let countActive: number = 0;
			for(const i of [x - 1, x, x + 1]) {
				for(const j of [y - 1, y, y + 1]) {
					for(const k of [z - 1, z, z + 1]) {
						for(const p of [w - 1, w, w + 1]) {
							if(i === x && j === y && k === z && p == w) {
								continue;
							}

							if(i < 0 || j < 0 || k < 0 || p < 0
								|| i >= this.totalSizeXY || j >= this.totalSizeXY || k >= this.totalSizeZW || p >= this.totalSizeZW) {
								countInactive += 1;
								continue;
							}

							if(this.data[i][j][k][p] === 1) {
								countActive += 1;
							} else {
								countInactive += 1;
							}
						}
					}
				}
			}

			if(this.data[x][y][z][w] === 1) {
				if(countActive === 2 || countActive === 3) {
					return 1;
				} else {
					return 0;
				}
			}

			if(countActive === 3) {
				return 1;
			}

			return 0;
		}
	}

	function part1(input: string[]): number {
		const space = new Space3D(input, 6);
		for(let i = 0; i < 6; ++i) {
			space.nextState();
		}

		return space.getActiveCount();
	}

	function part2(input: string[]): number {
		const space = new Space4D(input, 6);
		for(let i = 0; i < 6; ++i) {
			space.nextState();
		}

		return space.getActiveCount();
	}

	function task(input: string[]): void {
		console.log(`Part 1: ${part1(input)}`);
		console.log(`Part 2: ${part2(input)}`);
	}

	// task(testInput);
	task(input);
}
