export function task(): void {
	const testInput1: number[] = [0, 3, 6];
	const testInput2: number[] = [1, 3, 2];
	const testInput3: number[] = [2, 1, 3];
	const testInput4: number[] = [1, 2, 3];
	const testInput5: number[] = [2, 3, 1];
	const testInput6: number[] = [3, 2, 1];
	const testInput7: number[] = [3, 1, 2];

	const input: number[] = [2, 15, 0, 9, 1, 20];

	function findNth(input: number[], goalTurn: number): number {
		const lastUses = new Map<number, number>();
		for(let i = 0; i < input.length; ++i) {
			lastUses.set(input[i], i + 1);
		}

		const turns: number[] = [...input];
		for(let i = turns.length; i < goalTurn; ++i) {
			const turn = i + 1;
			const prev = turns[i - 1];

			let newNumber: number = 0;
			if(lastUses.has(prev)) {
				const lastUseTurn = lastUses.get(prev);
				if(lastUseTurn === turn - 1) {
					newNumber = 0;
				} else {
					newNumber = turn - 1 - lastUseTurn;
				}
			}

			turns.push(newNumber);
			lastUses.set(prev, turn - 1);
		}

		return turns[turns.length - 1];
	}

	function part1(input: number[]): number {
		return findNth(input, 2020);
	}

	function part2(input: number[]): number {
		return findNth(input, 30000000);
	}

	function task(input: number[]): void {
		console.log(`Part 1: ${part1(input)}`);
		console.log(`Part 2: ${part2(input)}`);
	}

	// task(testInput1);
	task(input);
}
