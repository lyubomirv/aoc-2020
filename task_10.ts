export function task(): void {
	const testInput1: number[] = [
		16,
		10,
		15,
		5,
		1,
		11,
		7,
		19,
		6,
		12,
		4];

	const testInput2: number[] = [
		28,
		33,
		18,
		42,
		31,
		14,
		46,
		20,
		48,
		47,
		24,
		23,
		49,
		45,
		19,
		38,
		39,
		11,
		1,
		32,
		25,
		35,
		8,
		17,
		7,
		9,
		4,
		2,
		34,
		10,
		3];

	const input: number[] = [
		71,
		30,
		134,
		33,
		51,
		115,
		122,
		38,
		61,
		103,
		21,
		12,
		44,
		129,
		29,
		89,
		54,
		83,
		96,
		91,
		133,
		102,
		99,
		52,
		144,
		82,
		22,
		68,
		7,
		15,
		93,
		125,
		14,
		92,
		1,
		146,
		67,
		132,
		114,
		59,
		72,
		107,
		34,
		119,
		136,
		60,
		20,
		53,
		8,
		46,
		55,
		26,
		126,
		77,
		65,
		78,
		13,
		108,
		142,
		27,
		75,
		110,
		90,
		35,
		143,
		86,
		116,
		79,
		48,
		113,
		101,
		2,
		123,
		58,
		19,
		76,
		16,
		66,
		135,
		64,
		28,
		9,
		6,
		100,
		124,
		47,
		109,
		23,
		139,
		145,
		5,
		45,
		106,
		41];

	function part1(input: number[]): number {
		let count1: number = 0;
		let count3: number = 1;
		for(let i = 0; i < input.length - 1; ++i) {
			const diff = input[i + 1] - input[i];
			if(diff === 1) {
				count1 += 1;
			} else if(diff === 3) {
				count3 += 1;
			}
		}

		return count1 * count3;
	}

	function part2(input: number[]): number {
		const storedCounts: number[] = [];
		for(const n of input) {
			storedCounts.push(0);
		}

		const count = (index: number): number => {
			if(index == input.length - 1) {
				return 1;
			}

			let counter: number = 0;
			const currentValue = input[index];
			for(let i = index + 1; i < input.length; ++i) {
				const diff = input[i] - currentValue;
				if(diff >= 1 && diff <= 3) {
					if(storedCounts[i] === 0) {
						storedCounts[i] = count(i);
					}
					counter += storedCounts[i];
				} else {
					continue;
				}
			}

			return counter;
		};

		return count(0);
	}

	function task(input: number[]): void {
		input.sort((a, b) => a - b);
		input.unshift(0);

		console.log(`Part 1: ${part1([...input])}`);
		console.log(`Part 2: ${part2([...input])}`);
	}

	task(input);
}
