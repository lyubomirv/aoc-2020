export function task(): void {
	const input: string[] = [];

	function part1(): number {
		return 0;
	}

	function part2(): number {
		return 0;
	}

	function task(input: string[]): void {
		console.log(`Part 1: ${part1()}`);
		console.log(`Part 2: ${part2()}`);
	}

	task(input);
}
